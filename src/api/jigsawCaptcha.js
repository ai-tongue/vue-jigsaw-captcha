import axios from 'axios'
// 超时时间
axios.defaults.timeout = 30000
// 跨域请求，允许保存cookie
axios.defaults.withCredentials = true

let base = '/backend/jigsaw/captcha'

/**
 * 注册验证码
 */
export const register = () => {
  return axios({
    url: `${base}/register`,
    method: 'get'
  })
}

/**
 * 拼图校验与登录
 */
export const check = form => {
  return axios({
    url: `${base}/check`,
    method: 'post',
    data: form
  })
}
