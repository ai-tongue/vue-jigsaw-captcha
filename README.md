# vue-jigsaw-captcha

#### 介绍
Vue编写的滑动拼图验证码前端示例

#### 操作截图
* 默认状态  
![open](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/open.png)
* 滑动中  
![slice](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/slice.png)
* 操作成功  
![success](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/success.png)
* 操作失败  
![error](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/error.png)

#### 软件架构
* vue: 2.5.2
* vue-router: 3.0.1
* axios: 0.18.0
* element-ui: 2.9.0
* animate.css: 3.7.2

#### 关连项目
后端示例：[jigsaw-captcha-demo](https://gitee.com/ai-tongue/jigsaw-captcha-demo)  
后端依赖：[jigsaw-captcha](https://gitee.com/ai-tongue/jigsaw-captcha)

#### 安装教程
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

#### 使用说明

##### Props
| Key | Type | Default | Description |
| --- | --- | --- | --- |
| show | Boolean | false | 是否显示验证码弹窗 | 
| router | String | '' | 验证成功后跳转路由 |
| register | Function | | 注册验证码方法 |
| login | Function | | 验证码校验及登录方法 |

##### Events
| Key | Attributes | Description |
| --- | --- | --- |
| close | | 关闭验证码弹窗 |
